#include <avr/io.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "ds1307.h"
#include "driver84x48.h"
#include "boolean.h"

//Dim laikas sekundemis (1800 sek = 30 min)
#define DIMTIME 1800L
#define DIMTIME100 180000L

const char menuData[][14] PROGMEM = {"Svie.&tams.l.",
                                          "Balta",
                                          "Melyna",
                                          "Raudona",
                                          "Laiko nustat.",
                                          ""};

enum SET_DATE_SELECTION {YEAR, MONTH, DAY, HOUR, MINUTE};
enum {MENU_DIMTIME, MENU_WHITE, MENU_BLUE, MENU_RED, MENU_SETDATE};

typedef struct{uint8_t hour, minute, second, year, month, day, weekday;} DATETIME;
typedef struct{uint8_t morning_h, morning_min, evening_h, evening_min,
                             white_morning_brightness, white_evening_brightness,
                             red_morning_brightness, red_evening_brightness,
                             blue_morning_brightness, blue_evening_brightness;} DATA;
uint8_t a, b;

uint8_t menu(/*const char *menuData[14]*/);
void setDateTime();
void standbyScreen();
void setDimTime();
void bar(uint8_t line, uint8_t status, BOOL selected);
void setBrightness(uint8_t *morning_, uint8_t *evening_, uint8_t *address, char *color);
void whitePWM(BOOL on);
void bluePWM(BOOL on);
void redPWM(BOOL on);
ISR(TIMER0_COMPA_vect);

DATA gData;

int main(void)
{
    unsigned char selectedMenuItem;
    DATETIME dt;

    gData.morning_h = eeprom_read_byte((uint8_t*)0x00);
    gData.morning_min = eeprom_read_byte((uint8_t*)0x01);
    gData.evening_h = eeprom_read_byte((uint8_t*)0x02);
    gData.evening_min = eeprom_read_byte((uint8_t*)0x03);
    if(gData.morning_h > 23 || gData.morning_min > 59 || gData.evening_h > 23|| gData.evening_min > 59){
        gData.morning_h = 7;
        gData.evening_h = 21;
        gData.morning_min = gData.evening_min = 0;
    }
    gData.white_morning_brightness = eeprom_read_byte((uint8_t*)0x04);
    if(gData.white_morning_brightness > 100) gData.white_morning_brightness = 70;
    gData.white_evening_brightness = eeprom_read_byte((uint8_t*)0x05);
    if(gData.white_evening_brightness > 100) gData.white_evening_brightness = 30;
    gData.red_morning_brightness = eeprom_read_byte((uint8_t*)0x06);
    if(gData.red_morning_brightness > 100) gData.red_morning_brightness = 70;
    gData.red_evening_brightness = eeprom_read_byte((uint8_t*)0x07);
    if(gData.red_evening_brightness > 100) gData.red_evening_brightness = 30;
    gData.blue_morning_brightness = eeprom_read_byte((uint8_t*)0x08);
    if(gData.blue_morning_brightness > 100) gData.blue_morning_brightness = 70;
    gData.blue_evening_brightness = eeprom_read_byte((uint8_t*)0x09);
    if(gData.blue_evening_brightness > 100) gData.blue_evening_brightness = 30;

    DDRD = 0xFF;

//NAUJAS:
    DDRB = 0x06;
    OCR1A = 0; //White
    OCR1B = 0; //Blue
    OCR2B = 0;  //Red

    TCCR1A = /*(1 << COM1A1) | (1 << COM1B1) |*/ (1 << WGM10) | (1 << WGM11);
    TCCR1B = (1 << WGM12) /*| (1 << CS10)*/ | (1 << CS11);

    TCCR2A = /*(1 << COM2A0) | */ /*(1 << COM2B1) | */ (1 << WGM20) | (1 << WGM21);
    TCCR2B = /*(1 << WGM22) | */ (1 << CS21) | (1 << CS20);

    /* CTC (Clear Timer on Compare match), prescaling: */
    TCCR0A = 0;
    TCCR0B = _BV(CS02) /*| _BV(CS01)*/ | _BV(CS00);
    OCR0A =  0xFF; //FF; //(F_CPU / 8UL) / speed;  //1000000
    TIMSK0 |= _BV(OCIE0A);

//SENAS:
//    OCR2A = 0; //63 - N/A
//    OCR2B = 0; //Red
//
//    OCR0A = 0; //White
//    OCR0B = 0; //Blue
//
//    TCCR0A = (1 << COM0A1) | (1 << COM0B1) | (1 << WGM00) | (1 << WGM01);
//    TCCR0B = /*(1 << WGM02) | */ (1 << CS00);
//
//    TCCR2A = /*(1 << COM2A0) | */ (1 << COM2B1) | (1 << WGM20) | (1 << WGM21);
//    TCCR2B = /*(1 << WGM22) | */ (1 << CS20);
//
//    /* CTC (Clear Timer on Compare match), prescaling: */
//    TCCR1A = 0;
//    TCCR1B = _BV(WGM12) | _BV(CS11) | _BV(CS10);
//    OCR1A =  0xFFFF; //(F_CPU / 8UL) / speed;  //1000000
//    TIMSK1 |= _BV(OCIE1A);

    LCD_init();
    LCD_clear();

    ds1307_init();
    ds1307_getdate(&dt.year, &dt.month, &dt.day, &dt.hour, &dt.minute, &dt.second);

    sei();

    while(1){
        selectedMenuItem = menu(/*pgm_read_ptr(&menuData[0][0])*/);
        LCD_clear();
        switch(selectedMenuItem){
            case MENU_DIMTIME: setDimTime(); break;
            //case MENU_WHITE: setBrightness(&a, &b, (uint8_t*)0x04, "BALTA");break;
            case MENU_WHITE: setBrightness(&(gData.white_morning_brightness), &(gData.white_evening_brightness), (uint8_t*)0x04, "BALTA");break;
            case MENU_BLUE: setBrightness(&gData.blue_morning_brightness, &gData.blue_evening_brightness, (uint8_t*)0x08, "MELYNA");break;
            case MENU_RED: setBrightness(&(gData.red_morning_brightness), &(gData.red_evening_brightness), (uint8_t*)0x06, "RAUDONA");break;
            case MENU_SETDATE: setDateTime(); break;
        }
        _delay_ms(200);
    }

    return 0;
}

uint8_t menu(/*const char *menuData[14]*/){
    uint8_t item = 0;
    uint8_t n, selection = 0;
    uint16_t inactive = 0;
    BOOL selected = FALSE;
    LCD_clear();
    do{
        if(pgm_read_byte(&menuData[item][0])){
            LCD_set_BankColumn(item, 5);
            for(n = 0; pgm_read_byte(&menuData[item][n]); n++){
                LCD_write_Char(pgm_read_byte(&menuData[item][n]), (selection == item) ? TRUE : FALSE);
            }
            item++;
        }
        else{
            while(((PINB & 0x11) | (PINC & 0x02)) == 0x13){
                inactive++;
                _delay_ms(20);
                if(inactive == 250){ //20 * 250 = 5000 ms
                    standbyScreen();
                    inactive = 0;
                    break;
                }
            }
            inactive = 0;
            if(!(PINB & 0x01)){ //UP
                if(selection) selection--;
            }
            else if(!(PINC & 0x02)){ //DOWN
                if(selection < item - 1) selection++;
            }
            else if(!(PINB & 0x10)){ //OK
                selected = TRUE;
            }
            item = 0;
            while(((PINB & 0x11) | (PINC & 0x02)) != 0x13);
        }
    }
    while(!selected);
    LCD_clear();
    return selection;
}

void standbyScreen(){
    DATETIME dt;
    char timeStr[]  = "HH:MM";
    char dateStr[] = "20YY-MM-DD";
    LCD_clear();
    while(((PINB & 0x19) == 0x19) && ((PINC & 0x06) == 0x06)){
        cli();
        ds1307_getdate(&dt.year, &dt.month, &dt.day, &dt.hour, &dt.minute, &dt.second);
        sei();
        timeStr[0] = dt.hour / 10 + 0x30;
        timeStr[1] = dt.hour % 10 + 0x30;
        timeStr[3] = dt.minute / 10 + 0x30;
        timeStr[4] = dt.minute % 10 + 0x30;

        LCD_write_2xString(2, 13, timeStr);

        dateStr[2] = dt.year / 10 + 0x30;
        dateStr[3] = dt.year % 10 + 0x30;
        dateStr[5] = dt.month / 10 + 0x30;
        dateStr[6] = dt.month % 10 + 0x30;
        dateStr[8] = dt.day / 10 + 0x30;
        dateStr[9] = dt.day % 10 + 0x30;

        LCD_write_String(4, 13, dateStr);
        _delay_ms(200);
    }
    _delay_ms(100);
    LCD_clear();
}

void setDateTime(){
    DATETIME dt;
    BOOL selected = FALSE;
    enum SET_DATE_SELECTION selection = YEAR;

    LCD_write_String(1, 12, "Data:");
    //LCD_write_String(2, 12, "20YY-MM-DD");
    LCD_write_String(3, 12, "Laikas:");
    //LCD_write_String(4, 12, "HH:MM");

    cli();
    ds1307_getdate(&dt.year, &dt.month, &dt.day, &dt.hour, &dt.minute, &dt.second);
    sei();
    if(dt.year < 15) dt.year = 15;

    do{
        LCD_set_BankColumn(2, 20);
        LCD_write_Char(dt.year / 10 + 0x30, (selection == YEAR));
        LCD_write_Char(dt.year % 10 + 0x30, (selection == YEAR));
        LCD_write_Char('-', FALSE);
        LCD_write_Char(dt.month / 10 + 0x30, (selection == MONTH));
        LCD_write_Char(dt.month % 10 + 0x30, (selection == MONTH) ? TRUE : FALSE);
        LCD_write_Char('-', FALSE);
        LCD_write_Char(dt.day / 10 + 0x30, (selection == DAY));
        LCD_write_Char(dt.day % 10 + 0x30, (selection == DAY));
        LCD_set_BankColumn(4, 20);
        LCD_write_Char(dt.hour / 10 + 0x30, (selection == HOUR));
        LCD_write_Char(dt.hour % 10 + 0x30, (selection == HOUR));
        LCD_write_Char(':', FALSE);
        LCD_write_Char(dt.minute / 10 + 0x30, (selection == MINUTE));
        LCD_write_Char(dt.minute % 10 + 0x30, (selection == MINUTE));
        ///while((PINB & 0x1F) == 0x13); Uzk 2016-01-12
        if(!(PINB & 0x01)){ //UP
            switch(selection){
            case YEAR:
                {
                    if(dt.year < 95)
                        dt.year++;
                    else
                        dt.year = 15;
                }
                break;
            case MONTH:
                {
                    if(dt.month < 12)
                        dt.month++;
                    else
                        dt.month = 1;
                }
                break;
            case DAY:
                {
                    if(dt.day < 31)  /// TODO: BUTINAI PAGALVOTI APIE MENESIU DIENAS PASKUTINES IR KELIAMUOSIUS METUS
                        dt.day++;
                    else
                        dt.day = 1;
                }
                break;
            case HOUR:
                {
                    if(dt.hour < 23)  /// TODO: BUTINAI PAGALVOTI APIE MENESIU DIENAS PASKUTINES IR KELIAMUOSIUS METUS
                        dt.hour++;
                    else
                        dt.hour = 0;
                }
                break;
            case MINUTE:
                {
                    if(dt.minute < 58)
                        dt.minute++;
                    else
                        dt.minute = 0;
                }
                break;
            }
            _delay_ms(200);
        }
        else if(!(PINC & 0x02)){ //DOWN
            switch(selection){
            case YEAR:
                {
                    if(dt.year > 15)
                        dt.year--;
                    else
                        dt.year = 95;
                }
                break;
            case MONTH:
                {
                    if(dt.month > 1)
                        dt.month--;
                    else
                        dt.month = 12;
                }
                break;
            case DAY:
                {
                    if(dt.day > 1)
                        dt.day--;
                    else
                        dt.day = 31; /// TODO: BUTINAI PAGALVOTI APIE MENESIU DIENAS PASKUTINES IR KELIAMUOSIUS METUS
                }
                break;
            case HOUR:
                {
                    if(dt.hour > 0)
                        dt.hour--;
                    else
                        dt.hour = 23;
                }
                break;
            case MINUTE:
                {
                    if(dt.minute > 0)
                        dt.minute--;
                    else
                        dt.minute = 59;
                }
                break;
            }
            _delay_ms(200);
        }
        else if(!(PINB & 0x10)){ //OK
            selected = TRUE;
        }
        else if(!(PINC & 0x04)){ //LEFT

            if(selection >= MONTH) selection--;
            else selection = MINUTE;
            while(!(PINC & 0x04));
        }
        else if(!(PINB & 0x08)){ //RIGHT
            if(selection < MINUTE) selection++;
            else selection = YEAR;
            while(!(PINB & 0x08));
        }
        //while((PINB & 0x13) != 0x13);
        _delay_ms(100);

    }
    while(!selected);

    ds1307_setdate(dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second);

    //LCD_write_Char(dt.year)
}

void setBrightness(uint8_t *morning_, uint8_t *evening_, uint8_t *address, char *color){
    BOOL selectedMorning = TRUE;
    uint8_t morning, evening;

    cli();

    morning = *morning_;
    evening = *evening_;

//morning = eeprom_read_byte((uint8_t*)0x04);
//evening = eeprom_read_byte((uint8_t*)0x05);
    _delay_ms(500);

///    if(morning > 100) morning = 70;
///    if(evening > 100) evening = 20;
    LCD_write_String(0, 20, color);
    LCD_write_String(1, 4, "Intens. max:");
    LCD_write_String(3, 4, "Intens. min:");
    bar(2, morning, TRUE);
    bar(4, evening, FALSE);

    while((PINB & 0x10)){
        if(!(PINC & 0x04)){ //LEFT
            if(selectedMorning){
                if(morning) morning--;
            }
            else{
                if(evening) evening--;
            }
        }
        else if(!(PINB & 0x08)){ //RIGHT
            if(selectedMorning){
                if(morning < 100) morning++;
            }
            else{
                if(evening < 100) evening++;
            }
        }
        else if((!(PINB & 0x01)) || (!(PINC & 0x02))){ //UP
            selectedMorning = !selectedMorning;
            while ((!(PINB & 0x01)) || (!(PINC & 0x02)));
        }

        bar(!selectedMorning ? 2 : 4, !selectedMorning?morning:evening, FALSE);
        bar(selectedMorning ? 2 : 4, selectedMorning?morning:evening, TRUE);

        *morning_ = morning;
        *evening_ = evening;

        if(selectedMorning){
            switch(color[0]){
                case 'R': {
                    OCR2B = 255*gData.red_morning_brightness/100;
                    break;
                }
                case 'B': {
                    OCR1A = (uint16_t)(0x03FFUL*gData.white_morning_brightness/100);
                    break;
                }
                case 'M': {
                    OCR1B = (uint16_t)(0x03FFUL*gData.blue_morning_brightness/100);
                }
            }
        }
        else{
            switch(color[0]){
                case 'R': {
                    OCR2B = 255*gData.red_evening_brightness/100;
                    break;
                }
                case 'B': {
                    OCR1A = (uint16_t)(0x03FFUL*gData.white_evening_brightness/100);
                    break;
                }
                case 'M': {
                    OCR1B = (uint16_t)(0x03FFUL*gData.blue_evening_brightness/100);
                }
            }
        }
        redPWM(OCR2B > 0);
        whitePWM(OCR1A > 0);
        bluePWM(OCR1B > 0);

        _delay_ms(100);
    }
    eeprom_update_byte(address, morning);
    eeprom_update_byte(address + 1, evening);
    ///*morning_ = morning;
    ///*evening_ = evening;
    //_delay_ms(100);
    sei();
}

void bar(uint8_t line, uint8_t status, BOOL selected){
    int x;
    LCD_set_BankColumn(line, 4);
    if(status / 10)
        LCD_write_Char(' ', TRUE);
    else
        LCD_write_Char(0x7C, FALSE);
    for(x = 20; x <= 90; x += 10){
        if(status / x)
            LCD_write_Char(' ', TRUE);
        else
            LCD_write_Char(0x7B, FALSE);
    }
    if(status == 100 /*/ 90*/)
        LCD_write_Char(' ', TRUE);
    else
        LCD_write_Char(0x7D, FALSE);

    if(status < 100){
        LCD_write_Char(' ', FALSE);
        LCD_write_Char(status / 10 + 0x30, selected);
        LCD_write_Char(status % 10 + 0x30, selected);
    }
    else{
        LCD_write_Char('1', selected);
        LCD_write_Char('0', selected);
        LCD_write_Char('0', selected);
    }
}

void setDimTime(){
///    uint8_t setPosition = 0;
    BOOL selected = FALSE;
    uint8_t selection = 0;
///    uint8_t morning_h, morning_min;
///    uint8_t evening_h, evening_min;

    LCD_write_String(1, 12, "Sviesejimas:");
    //LCD_write_String(2, 12, "HH:MM");
    LCD_write_String(3, 12, "Tamsejimas:");
    //LCD_write_String(4, 12, "HH:MM");
///TODO: GLOBALUS!!!
///    morning_h = eeprom_read_byte((uint8_t*)0x00);
///    morning_min = eeprom_read_byte((uint8_t*)0x01);
///    evening_h = eeprom_read_byte((uint8_t*)0x02);
///    evening_min  = eeprom_read_byte((uint8_t*)0x03);

    do{
        LCD_set_BankColumn(2, 20);
        LCD_write_Char(gData.morning_h / 10 + 0x30, selection == 0);
        LCD_write_Char(gData.morning_h % 10 + 0x30, selection == 0);
        LCD_write_Char(':', FALSE);
        LCD_write_Char(gData.morning_min / 10 + 0x30, selection == 1);
        LCD_write_Char(gData.morning_min % 10 + 0x30, selection == 1);
        LCD_set_BankColumn(4, 20);
        LCD_write_Char(gData.evening_h / 10 + 0x30, selection == 2);
        LCD_write_Char(gData.evening_h % 10 + 0x30, selection == 2);
        LCD_write_Char(':', FALSE);
        LCD_write_Char(gData.evening_min / 10 + 0x30, selection == 3);
        LCD_write_Char(gData.evening_min % 10 + 0x30, selection == 3);
        ///while((PINB & 0x1F) == 0x13); Uzk 2016-01-12
        if(!(PINB & 0x01)){ //UP
            switch(selection){
            case 0:
                {
                    if(gData.morning_h < 23){
                        gData.morning_h++;
                        if(gData.morning_h == 23 && gData.morning_min > 30){
                            gData.morning_min = 30;
                        }
                    }
                    else
                        gData.morning_h = 0;
                }
                break;
            case 2:
                {
                    if(gData.evening_h < 23){
                        gData.evening_h++;
                        if(gData.evening_h == 23 && gData.evening_min > 30){
                            gData.evening_min = 30;
                        }
                    }
                    else
                        gData.evening_h = 0;
                }
                break;
            case 1:
                {
                    if((gData.morning_min < 59) || (gData.evening_h == 23 && gData.evening_min < 30))
                        gData.morning_min++;
                    else
                        gData.morning_min = 0;
                }
                break;
            case 3:
                {
                    if((gData.evening_min < 59) || (gData.evening_h == 23 && gData.evening_min < 30))
                        gData.evening_min++;
                    else
                        gData.evening_min = 0;
                }
                break;
            }
            _delay_ms(200);
        }
        else if(!(PINC & 0x02)){ //DOWN
            switch(selection){
            case 0:
                {
                    if(gData.morning_h != 0)
                        gData.morning_h--;
                    else{
                        gData.morning_h = 23;
                        if(gData.morning_min > 30){
                            gData.morning_min = 30;
                        }
                    }
                }
                break;
            case 1:
                {
                    if(gData.morning_min)
                        gData.morning_min--;
                    else{
                        if(gData.morning_h < 23)
                            gData.morning_min = 59;
                        else
                            gData.morning_min = 30;
                    }
                }
                break;
            case 2:
                {
                    if(gData.evening_h != 0)
                        gData.evening_h--;
                    else{
                        gData.evening_h = 23;
                        if(gData.evening_min > 30){
                            gData.evening_min = 30;
                        }
                    }
                }
                break;
            case 3:
                {
                    if(gData.evening_min)
                        gData.evening_min--;
                    else{
                        if(gData.evening_h < 23)
                            gData.evening_min = 59;
                        else
                            gData.evening_min = 30;
                    }
                }
            }
            _delay_ms(200);
        }
        else if(!(PINB & 0x10)){ //OK
            selected = TRUE;
        }
        else if(!(PINC & 0x04)){ //LEFT
            if(selection) selection--;
            else selection = 3;
            while(!(PINC & 0x04));
        }
        else if(!(PINB & 0x08)){ //RIGHT
            if(selection < 3) selection++;
            else selection = 0;
            while(!(PINB & 0x08));
        }
        //while((PINB & 0x13) != 0x13);
        _delay_ms(100);
    }
    while(!selected);

    eeprom_update_byte((uint8_t*)0x00, gData.morning_h);
    eeprom_update_byte((uint8_t*)0x01, gData.morning_min);
    eeprom_update_byte((uint8_t*)0x02, gData.evening_h);
    eeprom_update_byte((uint8_t*)0x03, gData.evening_min);
///    _delay_ms(500);
}

void whitePWM(BOOL on){
    if(on){
        TCCR1A |= (1 << COM1A1);
    }
    else{
        TCCR1A &= ~(1 << COM1A1);
    }
}

void bluePWM(BOOL on){
    if(on){
        TCCR1A |= (1 << COM1B1);
    }
    else{
        TCCR1A &= ~(1 << COM1B1);
    }
}

void redPWM(BOOL on){
    if(on)
        TCCR2A |= (1 << COM2B1);
    else
        TCCR2A &= ~(1 << COM2B1);
}

ISR(TIMER0_COMPA_vect){
///    uint8_t hour, minute;
    int32_t currentTime, morningTime, eveningTime, morningActive, eveningActive;
    int32_t activeTime;
    uint16_t nextBlue = 0;
    uint8_t nextRed = 0;
    uint16_t nextWhite = 0;
//    int32_t z = (int32_t)(1800) * 100;
    DATETIME date;
    ds1307_getdate(&date.year, &date.month, &date.day, &date.hour, &date.minute, &date.second);
//    gData.morning_h = 10L;
//    gData.morning_min = 0;
    morningTime = gData.morning_h * 3600L + gData.morning_min * 60;
//    gData.evening_h = 21; //--
//    gData.evening_min = 0; //--
    eveningTime = gData.evening_h * 3600L + gData.evening_min * 60;
    currentTime = date.hour * 3600L + date.minute * 60 + date.second;

    morningActive = 0;//currentTime - morningTime;
    eveningActive = 0; //currentTime - eveningTime;

    if((currentTime <= morningTime) && (currentTime < eveningTime)){
        //Laikas nuo 00:00 iki ryto pradzios
        if(morningTime < eveningTime){
            morningActive = 0;
            eveningActive = DIMTIME;
        }
        else{
            morningActive = DIMTIME;
            eveningActive = 0;
        }
    }
    else if((currentTime > morningTime) && (currentTime < eveningTime)){
        //Laikas nuo ryto pradzios iki vakaro pradzios00
        if(morningTime < eveningTime){
            eveningActive = 0;
            morningActive = currentTime - morningTime; //19 - 10
            if(morningActive > DIMTIME) morningActive = DIMTIME;
        }
        else{
            morningActive = 0;
            eveningActive = eveningTime - currentTime; //19 - 10 //Pakeista 2016-03-08
            if(eveningActive > DIMTIME) eveningActive = DIMTIME;
        }
    }
    else if((currentTime > morningTime) && (currentTime >= eveningTime)){
        //Laikas nuo vakaro pradzios iki 00:00
        if(morningTime < eveningTime){
            morningActive = 0;
            eveningActive = currentTime - eveningTime;
            if(eveningActive > DIMTIME) eveningActive = DIMTIME;
        }
        else{
            eveningActive = 0;
            morningActive = currentTime - morningTime;
            if(morningActive > DIMTIME) morningActive = DIMTIME;
        }
    }
    //else nera
//gData.blue_morning_brightness = 90;
//gData.blue_evening_brightness = 100;

    activeTime = morningActive + eveningActive; //Vienas is kint. visad 0;

    if(morningActive > 0){
        nextBlue  = (uint16_t)((0x03FFUL*( ((int32_t)gData.blue_morning_brightness - (int32_t)gData.blue_evening_brightness) *activeTime+DIMTIME*(int32_t)gData.blue_evening_brightness) )/(DIMTIME100));
    }
    else{
        nextBlue  = (uint16_t)((0x03FFUL*(((int32_t)gData.blue_evening_brightness - (int32_t)gData.blue_morning_brightness)*activeTime+DIMTIME*(int32_t)gData.blue_morning_brightness))/(DIMTIME100));
    }
    if(morningActive > 0){
        nextRed   = (uint8_t)((255*(((int32_t)gData.red_morning_brightness - (int32_t)gData.red_evening_brightness)*activeTime+DIMTIME*(int32_t)gData.red_evening_brightness))/(DIMTIME100));
    }
    else{
        nextRed   = (uint8_t)((255*(((int32_t)gData.red_evening_brightness - (int32_t)gData.red_morning_brightness)*activeTime+DIMTIME*(int32_t)gData.red_morning_brightness))/(DIMTIME100));
    }
    if(morningActive > 0 /*gData.white_evening_brightness <= gData.white_morning_brightness*/){
        nextWhite = (uint16_t)((0x03FFUL*(((int32_t)gData.white_morning_brightness - (int32_t)gData.white_evening_brightness)*activeTime+DIMTIME*(int32_t)gData.white_evening_brightness))/(DIMTIME100));
    }
    else{
        nextWhite = (uint16_t)((0x03FFUL*(((int32_t)gData.white_evening_brightness - (int32_t)gData.white_morning_brightness)*activeTime+DIMTIME*(int32_t)gData.white_morning_brightness))/(DIMTIME100));
    }
    ///////////

/*    if(morningActive){
        nextBlue = 255*gData.blue_morning_brightness/100;
        nextRed = 255*gData.red_morning_brightness/100;
        nextWhite = 255*gData.white_morning_brightness/100;
    }
    else if(eveningActive){
        nextBlue = 255*gData.blue_evening_brightness/100;
        nextRed = 255*gData.red_evening_brightness/100;
        nextWhite = 255*gData.white_evening_brightness/100;
    } */

    whitePWM(nextWhite > 0);
    bluePWM(nextBlue > 0);
    redPWM(nextRed > 0);

    OCR2B = nextRed;   //Red
    OCR1A = nextWhite; //White
    OCR1B = nextBlue;  //Blue
    //LCD_write_Char('F', FALSE);
}
