// LED & LCD Show
// eDSP 2012 Vilnius
// AVR ATmega16

#ifndef driver84x48_h
#define driver84x48_h

//#define F_CPU 16000000UL
//#include <stdint.h>
//#include <stdlib.h>
#include <avr/io.h>
//#include <avr/iom48.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#include "boolean.h"

#define vcc             PORTD |= (1 << PD7)
#define gnd             PORTD &= ~(1 << PD3)

#define sce0            PORTD &= ~(1 << PD2)
#define res0            PORTD &= ~(1 << PD4)
#define dc0             PORTD &= ~(1 << PD2)
#define sdin0           PORTD &= ~(1 << PD1)
#define sclk0           PORTD &= ~(1 << PD0)
#define backled0        PORTD &= ~(1 << PD6)

#define sce1            PORTD |= (1 << PD2)
#define res1            PORTD |= (1 << PD4)
#define dc1             PORTD |= (1 << PD2)
#define sdin1           PORTD |= (1 << PD1)
#define sclk1           PORTD |= (1 << PD0)
#define backled1        PORTD |= (1 << PD6)

#define BANKS   6
#define COLUMNS 84

#define FONTWIDTH 5

void LCD_init(void);
void LCD_clear(void);
void LCD_set_BankColumn(unsigned char Bank, unsigned char Column);
void LCD_write_Byte(unsigned char dt, unsigned char command);
void LCD_write_Char(unsigned char c, BOOL inverted);
void LCD_write_2xChar(unsigned char Bank, unsigned char Column, unsigned char c);
void LCD_write_String(unsigned char Bank, unsigned char Column, char *s);
void LCD_write_2xString(unsigned char Bank, unsigned char Column, char *s);
#endif
