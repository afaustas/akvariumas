// LED & LCD Show
// eDSP 2012 Vilnius
// AVR ATmega16

#include "driver84x48.h"
#include "fonts.h"

void LCD_init(void) {
    DDRD = 255;
    vcc;
    gnd;
    backled1;
    sce1;
    res1;
    _delay_us(500);
    res0;
    _delay_ms(20);
    //{
    //    unsigned long int x;
    //    for(x = 0; x < 6000; x++);
    //}
    res1;
    _delay_us(500);
    LCD_write_Byte(0x21, 0);
    LCD_write_Byte(0b11001000, 0);
    LCD_write_Byte(0x20, 0);
    LCD_write_Byte(0x0C, 0);
    sce1;
}

void LCD_clear(void) {
    unsigned char t;
    unsigned char k;
    LCD_set_BankColumn(0, 0);
    for (t = 0; t < BANKS; t++) {
        for (k = 0; k < COLUMNS; k++) {
            LCD_write_Byte(0x00, 1);
        }
    }
    sce1;
}

void LCD_set_BankColumn(unsigned char Bank, unsigned char Column) {
    LCD_write_Byte(0b01000000 | Bank, 0);
    LCD_write_Byte(0b10000000 | Column, 0);
    sce1;
}

void LCD_write_Byte(unsigned char dt, unsigned char command) {
    unsigned char i;
    sce0;
    if (command)
        dc1;
    else
        dc0;
    for (i = 0; i < 8; i++) {
        if (dt & 0x80)
            sdin1;
        else
            sdin0;
        dt = dt << 1;
        sclk0;
        sclk1;
        sclk0;
    }
}

void LCD_write_Char(unsigned char c, BOOL inverted) {
    unsigned char line;
    for (line = 0; line < FONTWIDTH; line++)
        if(inverted){
            LCD_write_Byte(~pgm_read_byte(&(Font[c - 32][line])), 1);
        }
        else
            LCD_write_Byte(pgm_read_byte(&(Font[c - 32][line])), 1);
    if(((c == 0x7B)) && !inverted)
        LCD_write_Byte(0x81, 1);
    else
        LCD_write_Byte(inverted ? 0xFF : 0, 1);
}

void LCD_write_2xChar(unsigned char Bank, unsigned char Column, unsigned char c) {
    unsigned char row, column;
    for (row = 0; row < 2; row++) {
        for (column = 0; column < 2 * FONTWIDTH; column++)
            LCD_write_Byte(pgm_read_byte(&(number2x[c - 16 - 32][row + 2 * column])), 1);
        LCD_set_BankColumn(Bank + 1, Column);
    }
}

void LCD_write_String(unsigned char Bank, unsigned char Column, char *s) {
    LCD_set_BankColumn(Bank, Column);
    while (*s) {
        LCD_write_Char(*s, FALSE);
        s++;
    }
}

void LCD_write_2xString(unsigned char Bank, unsigned char Column, char *s) {
    int i = 0;
    while (*s) {
        LCD_set_BankColumn(Bank, Column + (2 * FONTWIDTH + 2) * i);
        LCD_write_2xChar(Bank, Column + (2 * FONTWIDTH + 2) * i, *s);
        s++;
        i++;
    }
}
